import { composeWithDevTools } from 'redux-devtools-extension';
import { createStore, applyMiddleware } from 'redux';
import promise from 'redux-promise';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger'
import reducer from '../reducers/.';

const logger = createLogger({
  colors: {
    title: () => 'inherit',
    prevState: () => '#9E9E9E',
    action: () => '#03A9F4',
    nextState: () => '#4CAF50',
    error: () => '#F20404'
  }
});

const store = createStore(reducer, composeWithDevTools(applyMiddleware(promise, thunk, logger)));

export default store;
