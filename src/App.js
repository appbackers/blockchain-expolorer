import React, { PureComponent } from 'react';
import { Route } from 'react-router-dom';

import Main from './pages/main';
import Block from './components/block/block.component';
import List from './components/list/list.component';
import Chart from './components/chart/Chart';

class App extends PureComponent {
  render() {
    return (
      <div>
        <Route path='/' component={Main} />
        <Route exact path='/' component={List} />
        <Route exact path='/chart' component={Chart} />
        <Route exact path='/blocks/:id' component={Block} />
      </div>
    )
  }
}

export default App;