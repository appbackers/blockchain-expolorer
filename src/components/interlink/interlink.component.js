import React from 'react';
import { Link } from 'react-router-dom';

const InterLink = ({interlinks}) => {
  return (
    <div>
      {interlinks.map((item) => {
        return (<div className='row block-info__row justify-content-between' key={item.id}>
          <div className='col'>{parseInt(item.level,0) + 1}</div>
          <Link className='col' to={`/blocks/${item.self_id}`}>{item.self_id}</Link>
        </div>);
      })}
    </div>
  );
}

export default InterLink;