import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import './list.component.css';
import moment from 'moment';

import InfiniteScroll from 'react-infinite-scroll-component';
import { connect } from 'react-redux';
import { getBlocks } from '../../actions/getBlocks';

class List extends PureComponent {
  constructor () {
    super();

    this.getAllBlocks = this.getAllBlocks.bind(this);
  }

  componentDidMount () {
    this.getAllBlocks()
  }

  getAllBlocks () {
    this.props.getBlocks();
  }

  renderBlocks () {
    let { blocks } = this.props;

    return (blocks || []).map((block, i) => {

      let date = moment.unix(Number(block.timestamp) / 1000).calendar(null, { sameElse: 'DD/MM/YYYY HH:mm' });

      return (
        <div className='block-list__wrapper block-list__item' key={i}>
          <div className='block-list__element block-list__element--height'>#{block.height}</div>
          <div className='block-list__element block-list__element--timestamp'>{date}</div>
          <div className='block-list__element block-list__element--id'>
            <Link to={`/blocks/${block.id}`}>{block.id}</Link>
          </div>
          <div className='block-list__element'>
            {block.votes}
          </div>
          <div className='block-list__element block-list__element--transactionsCount'>{block.transactionsCount}</div>
        </div>);
    });
  }

  render () {
    return (
      <div className='block-list'>
        <InfiniteScroll
          dataLength={this.props.blocks.length}
          next={this.getAllBlocks}
          hasMore={true}
        >
          <div className='block-list__wrapper block-list__header'>
            <div className='block-list__element block-list__element--height'>Height</div>
            <div className='block-list__element block-list__element--timestamp'>Time</div>
            <div className='block-list__element block-list__element--id'>
              Hash
            </div>
            <div className='block-list__element'>
              Relayed By
            </div>
            <div className='block-list__element block-list__element--transactionsCount'>TX Number</div>
          </div>

          {this.renderBlocks()}
        </InfiniteScroll>
      </div>);
  }

}

const mapStateToProps = state => {
  return {
    blocks: state.blocks.data,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    getBlocks: () => dispatch(getBlocks()),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(List);