import React from 'react';
import './transaction.component.css';

const Transaction = ({ data }) => {
  return (
    <div className='transaction'>
      <div className='transaction__header row justify-content-between'>
        <div>Transaction: {data[0].transaction_id}</div>
      </div>
      <div className='row'>
        <div className='col'>
          {
            data.filter(transaction => !transaction.value).map((transaction, i) => {
              return <div key={transaction.id}>
                <div><strong>Address:</strong> {transaction.address_id}</div>
              </div>
            })
          }
        </div>

        <div className='col'>
          {
            data.filter(transaction => transaction.value).map((transaction, i) => {
              return <div key={transaction.id}>
                <div><strong>Address:</strong> {transaction.address_id}</div>
                <div><strong>Value:</strong> {transaction.value}</div>
              </div>
            })
          }
        </div>
      </div>
    </div>
  )
}

export default Transaction;