import React, { PureComponent } from 'react';

import './header.css';
import Link from 'react-router-dom/es/Link';
import { searchBlocks, resetOffset } from '../../actions/getBlocks';
import { connect } from 'react-redux';
import _ from 'lodash';

class Header extends PureComponent {
  constructor () {
    super();

    this.searchBlocks = _.debounce(this.searchBlocks.bind(this), 300);
  }

  componentDidMount () {
    let search = new URLSearchParams(this.props.location.search).get('search');

    if (search && search !== this.input.value) {
      this.input.value = search;

      return this.props.searchBlocks(this.input.value);
    }
  }

  componentWillReceiveProps (props) {
    let search = new URLSearchParams(props.location.search).get('search');

    if (!search) {
      return this.input.value = '';
    }
  }

  searchBlocks () {
    if (!this.input.value) {
      this.props.resetOffset();
      return this.props.history.push('/');
    }

    this.props.searchBlocks(this.input.value);

    return this.props.history.push(`/?search=${this.input.value}`);
  }

  render () {

    return (
      <header className='header'>
        <Link to='/' className='header__logo'>EXPLORER</Link>

        <ul>
          <li>
            <input
              className='header__search'
              ref={input => this.input = input}
              onChange={this.searchBlocks}
              name='search'
              type='text'
              placeholder='search header...'
            />
          </li>
        </ul>
      </header>
    );
  }
}

const mapStateToProps = state => {
  return {
    blocks: state.blocks.data,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    searchBlocks: (query) => dispatch(searchBlocks(query)),
    resetOffset: () => dispatch(resetOffset()),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);