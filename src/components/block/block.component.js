import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { getBlock } from '../../actions/getBlock';
import { getTransactions } from '../../actions/getTransactions';
import { getInterlinks } from '../../actions/getInterlinks';
import Preloader from '../preloader/preloader.component';
import Transaction from '../../components/transaction/transaction.component';
import Interlinks from '../../components/interlink/interlink.component';
import './block.component.css';

class Block extends PureComponent {
  componentDidMount () {
    this.props.getBlock(this.props.match.params.id);
    this.props.getTransactions(this.props.match.params.id);
    this.props.getInterlinks(this.props.match.params.id);
  }

  componentWillReceiveProps (props) {
    if (this.props.match.params.id !== props.match.params.id) {
      this.props.getBlock(props.match.params.id);
      this.props.getTransactions(props.match.params.id);
      this.props.getInterlinks(props.match.params.id);
    }
  }

  renderTransactions (transactions) {
    return Object.keys(transactions).map((id, i) => <Transaction key={i} data={transactions[id]}/>)
  }

  render () {
    let { block, fetching, interlinks, transactions } = this.props;

    return (
      !block ?
        <Preloader/>
        :
        <div className='container block-element'>
          <h2>Block #{block.height}</h2>
          <div className='block-info'>
            <div className='row justify-content-between block-info__thead'>
              <div className='col'>Summary</div>
            </div>

            <div className='row block-info__row justify-content-between'>
              <div className='col'>Hash</div>
              <div className='col'>{block.id}</div>
            </div>


            <div className='row block-info__row justify-content-between'>
              <div className='col'>Relayed by</div>
              <div className='col'>{block.votes}</div>
            </div>


            <div className='row block-info__row justify-content-between'>
              <div className='col'>Difficulty</div>
              <div className='col'>{block.difficulty}</div>
            </div>


            <div className='row block-info__row justify-content-between'>
              <div className='col'>Nonce</div>
              <div className='col'>{block.nonce}</div>
            </div>


            <div className='row block-info__row justify-content-between'>
              <div className='col'>Nonce bits</div>
              <div className='col'>{block.n_bits}</div>
            </div>

            <div className='row block-info__row justify-content-between'>
              <div className='col'>State root</div>
              <div className='col'>{block.state_root}</div>
            </div>

            <div className='row block-info__row justify-content-between'>
              <div className='col'>Parent</div>
              <div className='col'><Link to={`/blocks/${block.parent_id}`}>{block.parent_id}</Link></div>
            </div>

            {block.next_id ? (<div className='row block-info__row justify-content-between'>
              <div className='col'>Next</div>
              <div className='col'><Link to={`/blocks/${block.next_id}`}>{block.next_id}</Link></div>
            </div>) : null
            }


            <div className='row block-info__row justify-content-between'>
              <div className='col'>Transactions root</div>
              <div className='col'>{block.transactions_root}</div>
            </div>

            <div className='row block-info__row justify-content-between'>
              <div className='col'>Ad proofs root</div>
              <div className='col'>{block.ad_proofs_root}</div>
            </div>


            <div className='row block-info__row justify-content-between'>
              <div className='col'>Equihash solutins</div>
              <div className='col'>{block.equihash_solutions.join(' ')}</div>
            </div>

          </div>


          <div className='interlinks'>
            <h2 className='interlinks__title'>Interlinks</h2>
            {interlinks && interlinks.length !== 0 ? <Interlinks interlinks={interlinks}/> : <p>No interlinks</p>}
          </div>

          <br/>

          <div className='transactions'>
            <h2 className='transactions__title'>Transactions</h2>
            {this.renderTransactions(transactions)}
            {!transactions || transactions.length === 0 ? <p>No Transactions</p> : null}
          </div>
        </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    block: state.currentBlock.data,
    fetching: state.currentBlock.fetching,
    transactions: state.transactions.data,
    interlinks: state.interlinks.data,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    getBlock: id => dispatch(getBlock(id)),
    getTransactions: id => dispatch(getTransactions(id)),
    getInterlinks: id => dispatch(getInterlinks(id)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Block);