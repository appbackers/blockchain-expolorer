import React, { PureComponent } from 'react';
import { URL_GET_BLOCKS } from '../../constants';

import Highcharts from 'highcharts';

import Preloader from '../preloader/preloader.component';

export default class Chart extends PureComponent {
  constructor () {
    super();
    this.state = {
      config: null,
    }
  }

  componentDidMount () {
    fetch(`${URL_GET_BLOCKS}/chart/blocks`)
      .then(data => data.json())
      .then((data) => {
        let config = {
          title: {text: 'Blocks by date'},
          chart: {
            type: 'spline',
          },
          xAxis: {
            type: 'datetime',
            dateTimeLabelFormats: { // don't display the dummy year
              month: '%b',
              year: '%b'
            },
            title: {
              text: 'Date'
            }
          },
          series: [
            {
              name: 'Blocks',
              data: data.map((item) => {
                return [item[0], parseInt(item[1])]
              }),
            },
          ],
        };

        console.log(config.series)

        Highcharts.chart(this.container, config);
      });
  }

  render () {
    let { config } = this.state;

    return <div>
      <div className='chart' ref={el => this.container = el}/>
    </div>
  }
}