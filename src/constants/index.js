// urls
export const URL_GET_BLOCKS = 'http://f6110111.ngrok.io/blocks';

// blocks
export const CLEAR_BLOCKS       = 'CLEAR_BLOCKS';
export const GET_BLOCKS         = 'GET_BLOCKS';
export const GET_BLOCKS_SUCCESS = 'GET_BLOCKS_SUCCESS';
export const GET_BLOCKS_FAIL    = 'GET_BLOCKS_FAIL';

// block
export const GET_BLOCK         = 'GET_BLOCK';
export const GET_BLOCK_SUCCESS = 'GET_BLOCK_SUCCESS';
export const GET_BLOCK_FAIL    = 'GET_BLOCK_FAIL';

// block transactions
export const GET_TRANSACTIONS = 'GET_TRANSACTIONS';
export const GET_TRANSACTIONS_SUCCESS = 'GET_TRANSACTIONS_SUCCESS';
export const GET_TRANSACTIONS_FAIL = 'GET_TRANSACTIONS_FAIL';
// search blocks
export const SEARCH_BLOCK         = 'SEARCH_BLOCK';
export const SEARCH_BLOCK_SUCCESS = 'SEARCH_BLOCK_SUCCESS';
export const SEARCH_BLOCK_FAIL    = 'SEARCH_BLOCK_FAIL';

export const GET_INTERLINKS = 'GET_INTERLINKS';
export const GET_INTERLINKS_SUCCESS = 'GET_INTERLINKS_SUCCESS';
export const GET_INTERLINKS_FAIL = 'GET_INTERLINKS_FAIL';