import { 
  GET_BLOCK, 
  GET_BLOCK_SUCCESS, 
  GET_BLOCK_FAIL, 
  URL_GET_BLOCKS
} from '../constants';

export const getBlock = id => dispatch => {
  dispatch({
    type: GET_BLOCK,
    payload: {
      fetching: 'loading'
    }
  })

  fetch(`${URL_GET_BLOCKS}/${id}`)
    .then(response => {
      if (response.status >= 200 && response.status < 300) {
        return Promise.resolve(response)
      } else {
        return Promise.reject(new Error(response.statusText))
      }
    })
    .then(res => res.json())
    .then(data => {
      dispatch({
        type: GET_BLOCK_SUCCESS,
        payload: {
          data,
          fetching: true
        },
      })
    })
    .catch(err => {
      dispatch({
        type: GET_BLOCK_FAIL,
        payload: {
          message: err,
          fetching: false
        },
      })
    })
}