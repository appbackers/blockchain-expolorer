import { 
  GET_TRANSACTIONS, 
  GET_TRANSACTIONS_SUCCESS, 
  GET_TRANSACTIONS_FAIL, 
  URL_GET_BLOCKS
} from '../constants';

export const getTransactions = id => dispatch => {
  dispatch({
    type: GET_TRANSACTIONS,
    payload: {
      fetching: 'loading'
    }
  })

  fetch(`${URL_GET_BLOCKS}/${id}/transactions`)
    .then(response => {
      if (response.status >= 200 && response.status < 300) {
        return Promise.resolve(response)
      } else {
        return Promise.reject(new Error(response.statusText))
      }
    })
    .then(res => res.json())
    .then(data => {
      dispatch({
        type: GET_TRANSACTIONS_SUCCESS,
        payload: {
          data,
          fetching: true
        },
      })
    })
    .catch(err => {
      dispatch({
        type: GET_TRANSACTIONS_FAIL,
        payload: {
          message: err,
          fetching: false
        },
      })
    })
}