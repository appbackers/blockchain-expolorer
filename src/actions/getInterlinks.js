import { 
  GET_INTERLINKS, 
  GET_INTERLINKS_SUCCESS, 
  GET_INTERLINKS_FAIL, 
  URL_GET_BLOCKS
} from '../constants';

export const getInterlinks = id => dispatch => {
  dispatch({
    type: GET_INTERLINKS,
    payload: {
      fetching: 'loading'
    }
  })

  fetch(`${URL_GET_BLOCKS}/${id}/interlinks`)
    .then(response => {
      if (response.status >= 200 && response.status < 300) {
        return Promise.resolve(response)
      } else {
        return Promise.reject(new Error(response.statusText))
      }
    })
    .then(res => res.json())
    .then(data => {
      dispatch({
        type: GET_INTERLINKS_SUCCESS,
        payload: {
          data,
          fetching: true
        },
      })
    })
    .catch(err => {
      dispatch({
        type: GET_INTERLINKS_FAIL,
        payload: {
          message: err,
          fetching: false
        },
      })
    })
}