import {
  GET_BLOCKS,
  GET_BLOCKS_SUCCESS,
  GET_BLOCKS_FAIL,
  CLEAR_BLOCKS,
  SEARCH_BLOCK,
  SEARCH_BLOCK_SUCCESS,
  SEARCH_BLOCK_FAIL,
  URL_GET_BLOCKS,
} from '../constants';

let offset = 0;
let search = false;

export const resetOffset = () => dispatch => {
  offset = 0;
  search = false;

  dispatch({
    type: CLEAR_BLOCKS,
  });

  getBlocks()(dispatch);
};

export const getBlocks = () => dispatch => {
  if (search) {
    return;
  }

  dispatch({
    type: GET_BLOCKS,
    payload: {
      fetching: 'loading',
    },
  });

  let url = `${URL_GET_BLOCKS}?offset=${offset}&limit=100`;

  offset += 100;

  fetch(url)
    .then(response => {
      if (response.status >= 200 && response.status < 300) {
        return Promise.resolve(response)
      } else {
        return Promise.reject(new Error(response.statusText))
      }
    })
    .then(res => res.json())
    .then(data => {
      dispatch({
        type: GET_BLOCKS_SUCCESS,
        payload: {
          data,
          fetching: true,
        },
      })
    })
    .catch(err => {
      dispatch({
        type: GET_BLOCKS_FAIL,
        payload: {
          message: err,
          fetching: false,
        },
      })
    })
};

export const searchBlocks = (query) => dispatch => {
  dispatch({
    type: SEARCH_BLOCK,
    payload: {
      fetching: 'loading',
    },
  });

  search = true;

  let url = `${URL_GET_BLOCKS}/search?query=${query}`;

  fetch(url)
    .then(response => {
      if (response.status >= 200 && response.status < 300) {
        return Promise.resolve(response)
      } else {
        return Promise.reject(new Error(response.statusText))
      }
    })
    .then(res => res.json())
    .then(data => {
      dispatch({
        type: SEARCH_BLOCK_SUCCESS,
        payload: {
          data,
          fetching: true,
        },
      })
    })
    .catch(err => {
      dispatch({
        type: SEARCH_BLOCK_FAIL,
        payload: {
          message: err,
          fetching: false,
        },
      })
    })
}