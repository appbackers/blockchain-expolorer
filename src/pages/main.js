import React, { PureComponent } from 'react';

import List from '../components/list/list.component';
import Header from '../components/header/header.component';

class Main extends PureComponent {
  componentDidMount () {
  }

  render () {
    return (
      <div>
        <Header history={this.props.history} location={this.props.location}/>
        <main className='main'>
          {this.props.children}
        </main>
      </div>
    );
  }
}

export default Main;