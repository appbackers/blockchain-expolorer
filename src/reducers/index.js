import { combineReducers } from 'redux'
import blocks from './blocks';
import currentBlock from './currentBlock';
import transactions from './transactions';
import interlinks from './interlinks';

export default combineReducers({
  blocks,
  currentBlock,
  transactions,
  interlinks
})