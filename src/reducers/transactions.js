import { 
  GET_TRANSACTIONS, 
  GET_TRANSACTIONS_SUCCESS, 
  GET_TRANSACTIONS_FAIL 
} from '../constants';

const initialState = {
  data: [],
};

const groupById = transactions => {
  let group = {}
  transactions.forEach(transaction => {
    if (!group[transaction.transaction_id]) {
      group[transaction.transaction_id] = [];
    }

    group[transaction.transaction_id].push(transaction);
  });

  return group;
};

const reducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case GET_TRANSACTIONS: 
      return Object.assign({}, state, {
        fetching: payload.fetching
      });

    case GET_TRANSACTIONS_SUCCESS:
      return Object.assign({}, state, {
        fetching: payload.fetching,
        data: {...groupById(payload.data)}
      });

    case GET_TRANSACTIONS_FAIL:
      return Object.assign({}, state, {
        fetching: payload.fetching,
        message: payload.message
      });

    default: return state;
  }
}

export default reducer;