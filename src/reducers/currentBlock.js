import { 
  GET_BLOCK, 
  GET_BLOCK_SUCCESS, 
  GET_BLOCK_FAIL 
} from '../constants';

const initialState = {};

const reducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case GET_BLOCK: 
      return Object.assign({}, state, {
        fetching: payload.fetching
      });

    case GET_BLOCK_SUCCESS:
      return Object.assign({}, state, {
        fetching: payload.fetching,
        data: payload.data
      });

    case GET_BLOCK_FAIL:
      return Object.assign({}, state, {
        fetching: payload.fetching,
        message: payload.message
      });

    default: return state;
  }
}

export default reducer;