import { 
  GET_INTERLINKS, 
  GET_INTERLINKS_SUCCESS, 
  GET_INTERLINKS_FAIL 
} from '../constants';

const initialState = {};

const reducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case GET_INTERLINKS: 
      return Object.assign({}, state, {
        fetching: payload.fetching
      });

    case GET_INTERLINKS_SUCCESS:
      return Object.assign({}, state, {
        fetching: payload.fetching,
        data: payload.data
      });

    case GET_INTERLINKS_FAIL:
      return Object.assign({}, state, {
        fetching: payload.fetching,
        message: payload.message
      });

    default: return state;
  }
}

export default reducer;