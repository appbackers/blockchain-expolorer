import {
  GET_BLOCKS,
  SEARCH_BLOCK,
  GET_BLOCKS_SUCCESS,
  CLEAR_BLOCKS,
  GET_BLOCKS_FAIL,
  SEARCH_BLOCK_SUCCESS,
} from '../constants';

const initialState = {
  data: [],
};

const reducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case CLEAR_BLOCKS: {
      return { ...state, data: [] };
    }

    case GET_BLOCKS:
      return Object.assign({}, state, {
        fetching: payload.fetching,
      });

    case SEARCH_BLOCK:
      return Object.assign({}, state, {
        fetching: payload.fetching,
      });

    case GET_BLOCKS_SUCCESS:
      return Object.assign({}, state, {
        fetching: payload.fetching,
        data: [...state.data, ...payload.data],
      });

    case SEARCH_BLOCK_SUCCESS:
      return Object.assign({}, state, {
        fetching: payload.fetching,
        data: payload.data,
      });

    case GET_BLOCKS_FAIL:
      return Object.assign({}, state, {
        fetching: payload.fetching,
        message: payload.message,
      });

    default:
      return state;
  }
};

export default reducer;